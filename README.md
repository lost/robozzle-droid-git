RoboZZle Droid is a programming game, where you make a robot, that navigates a 2D map.

The game was recently featured in the [Top Underrated educational games](https://gameskeys.net/top-underrated-educational-games-to-play/)